// Nodejs encryption with CTR
var crypto = require('crypto');
var ourCrypto = require('../node_modules/common/lsi/ourCrypto');

// var keyB;
//
// generateKeysB=function()
// {
//   console.log("Start Generate keys B");
//   keyB = ourCrypto.generateKeys();
//
//   //console.log("Public Key : " ,diffHellA.getPublicKey('base64'));
//   //console.log("Private Key : " ,diffHellA.getPrivateKey('base64'));
//   //console.log("Public Key : " ,diffHellA.getPublicKey('hex'));
//   //console.log("Private Key : " ,diffHellA.getPrivateKey('hex'));
//   //return diffHellA;
// }
//
// var encryptStringWithRsaPublicKey = function(toEncrypt, publicKey) {
//     //var absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey);
//     //var publicKey = fs.readFileSync(absolutePath, "utf8");
//     //var buffer = new Buffer(toEncrypt);
//     var encrypted = ourCrypto.encryptStringWithRsaPublicKey(toEncrypt, publicKey);
//     return encrypted;
// };
//
// var decryptStringWithRsaPrivateKey = function(toDecrypt, privateKey) {
//     //var absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey);
//     //var privateKey = fs.readFileSync(absolutePath, "utf8");
//     //var buffer = new Buffer(toDecrypt, "base64");
//     var decrypted = ourCrypto.decryptStringWithRsaPrivateKey(toDecrypt, privateKey);
//     return decrypted;
// };

var encryptKeyVal=function(key, val)
{
  var secret=ourCrypto.GenerateSecretByKey(key);
  var res={};
  res['dppEncode']=ourCrypto.encryptKeyVal(secret, val);
  const hmac = crypto.createHmac('sha256', secret);
  hmac.update(res['dppEncode']);
  res['dppDigest']=hmac.digest().toString('hex');
  return res;
}

var decryptKeyVal=function(key, val, digets)
{
  var secret=ourCrypto.GenerateSecretByKey(key);
  const hmac = crypto.createHmac('sha256', secret);
  hmac.update(val);
  //console.log(hmac.digest().toString('hex'));
  if(digets===hmac.digest().toString('hex'))
  {
    return ourCrypto.decryptKeyVal(secret, val);
  }
  return '-9999'
}

module.exports.text2Hash=ourCrypto.text2Hash;
//module.exports.encryptStringWithRsaPublicKey=encryptStringWithRsaPublicKey;
//module.exports.decryptStringWithRsaPrivateKey=decryptStringWithRsaPrivateKey;
//module.exports.generateKeysB=generateKeysB;
module.exports.encryptKeyVal=encryptKeyVal;
module.exports.decryptKeyVal=decryptKeyVal;
//module.exports.encryptKeyValPrivatePublic=ourCrypto.encryptKeyValPrivatePublic;
//module.exports.decryptKeyValPrivatePublic=ourCrypto.decryptKeyValPrivatePublic;

//start test Block
//var enc_1=encryptStringWithRsaPublicKey("test string to encrypt", './key/keyPubB.pem');
//console.log(enc_1);
//var dec_1=decryptStringWithRsaPrivateKey(enc_1, './key/keyPrivB.pem');
//console.log(dec_1);

//end test Block
