var restify = require('restify');
var fetch = require('node-fetch');
var FetchStream = require("fetch").FetchStream;

var logscreator = require('./node_modules/common/sl/logsworks');
var createlogs = logscreator.createlogs;

var testredis = require("redis"),
    testclient = testredis.createClient(6379, "redis");
    testclient.on("connect", function (err) {
        console.log("Redis is online ");
    });
    testclient.on("error", function (err) {
        console.log("Error " + err);
    });



process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
// var path = require("path");
var fs = require("fs");

var ourCryptoB = require('./lsi/ourCryptoServerB');

// This line is from the Node.js HTTPS documentation.
var httpsServerOptions   = {
    key          : fs.readFileSync('./key/keySSL.pem'),
    certificate   : fs.readFileSync('./key/certSSL.pem'),
    insecure: true
};
var count = 0;
var countAll = 0;
var countMainReq_1 = 0;
var countMainReq_2 = 0;
var serverB = restify.createServer(httpsServerOptions);
//var serverB = restify.createServer();

serverB.use(restify.plugins.acceptParser(serverB.acceptable));
serverB.use(restify.plugins.jsonp());
serverB.use(restify.plugins.bodyParser({ mapParams: false }));

serverB.use(
  function crossOrigin(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    return next();
  }
);

/*serverB.get({path: '/benchmarking/:sessionid'}, function(req, res, next) {
  var sessionid = req.params.sessionid;
  fs.exists(`./benchmarking/${sessionid}.json`, (exists) => {
    if(exists) {
      fs.readFile(`./benchmarking/${sessionid}.json`, 'utf8', function readFileCallback(err, data){
        var info = {};
        if(data) info = JSON.parse(data);
        var s = 0;
        var min = 0;

        for(let key in info){
          if(key !== 'Data length' && key !== 'sessionid')
            if(s==0) min = info[key];
            else if(min>info[key]) min=info[key];
          s++;
        }
        for(let key in info) if(key !== 'Data length' && key !== 'sessionid')  info[key]-=min;
        res.send(info);
      });
    }
  });

});
serverB.get({path: '/benchmarkingresults/all'}, function(req, res, next) {
 var result=[];
  fetch('https://ec2-18-184-89-21.eu-central-1.compute.amazonaws.com:49161/benchmarkingresults/all',
  {
    method: "GET"
  })  .then(function(res) {
    return res.json();
  }).then(function(json){
    fs.readdirSync("./benchmarking/").forEach((file,index,arr) => {
      if(arr.length > 0) {
      fs.readFile(`./benchmarking/${file}`, 'utf8', function readFileCallback(err, data){
        var info = {};

        if(data) info = JSON.parse(data);
        var infoServerA = json.filter(function(el){
          return parseInt(  file.split('.')[0] ) === parseInt( el.sessionid )
        })

        var infoAB = Object.assign({},info,infoServerA[0]);
        var s = 0;
        var min = 0;
        for(let key in infoAB){
          if(key !== 'Data length' && key !== 'sessionid')
            if(s==0) min = infoAB[key];
            else if(min>infoAB[key]) min=infoAB[key];
          s++;
        }

        var minDate = new Date(min).toISOString();

        for(let key in infoAB) if(key !== 'Data length' && key !== 'sessionid') infoAB[key]-=min;
        infoAB["sessionid"]=file.split('.')[0];
        infoAB['Time Start Request'] = minDate;
        result.push(infoAB);
        if(index === arr.length-1)
          res.send(result);
      });
    } else {
      res.send([]);
    }
    });
  })

});
*/
serverB.post({path: '/sessions/:sessionid/DPPs'}, function(req, res, next) {
  countAll++;

  console.log("log in post before hasOwnProperty");
  var dataSession = {};
  var clientreq = true;
  var data = null;
  var deteteTimeout =  15000;

  console.log(req.body);
  var sessionid = req.params.sessionid;
  console.log(sessionid)
  /*fs.exists(`./benchmarking/${sessionid}.json`, (exists) => {
    if(!exists) fs.writeFile(`./benchmarking/${sessionid}.json`, '', 'utf8',function(){}); // write it back
  });*/

  
  if(req.body === undefined) return next();
  if(!req.body.hasOwnProperty("json")){
    if(req.body.data.hasOwnProperty("ClientID") && req.body.data.hasOwnProperty("Referrer") && req.body.data.hasOwnProperty("Host")){
      var start = new Date().getTime();

      var redis = require("redis"),
      client = redis.createClient(6379, "redis"),
      client2 = redis.createClient(6379, "redis");
      console.log("log in if before redis");
      client.on("connect", function (err) {
        console.log("Redis is online ");
        });
        client.on("error", function (err) {
            console.log("Error " + err);
        });
        client2.on("connect", function (err) {
        console.log("Redis is online ");
        });
        client2.on("error", function (err) {
        console.log("Error " + err);
        });
      var unsub = false;
      var del = false;
      clientreq = false;
      var ressend = false;
      dataSession = {
        ClientID:req.body.data.ClientID,
        Referrer:req.body.data.Referrer,
        Host:req.body.data.Host
      };


      data = hashing(req.body.data.encodeB);
      var timeafterhashing = new Date().getTime();
      sendLogsToPSB(createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'UI action','Info','B','','true'),res,ressend);
      var prom =[];
      var ret;
      var eventid;
      var timegetinfofromPSBServerA;

      client2.on('subscribe', function (pattern, count) {
        console.log('client from server A psubscribed to ' + pattern + ', ' + count + ' total subscriptions');
        if(!del) {
          client.publish(`${sessionid}_serverA`, "done", redis.print);
        }
    //     setTimeout(function(){
    //       client2.ping("PING",function(err,reply){
    //  //       console.log(reply);
    //         if(reply){
    //           console.log("Timeout!!!! UP!!!!");
    //           console.log(ressend);
    //           if(!unsub) {
    //             client2.unsubscribe();
    //             unsub = true
    //           }
    //           if(!del) {
    //             client.del(`${sessionid}_serverA`);
    //             del = true
    //           }

    //           client.quit();
    //           client2.quit();
    //           del = true
    //           if(!ressend){
    //             console.log("Time out sending....")
    //             res.send({msg:'Timeout'});
    //             ressend = true;
    //           }
    //         }
    //         if(err){
    //           if(!ressend){
    //             res.send({msg:'Error connection'});
    //             ressend = true;
    //           }
    //         }
    //       })
    //     },deteteTimeout);
      });
      client2.on("message", function (channel, message) {
        console.log("sub channel " + channel + ": " + message);
        if(message!=='done' && message!=='error'){
            console.log(message);
            timegetinfofromPSBServerA = new Date().getTime();
            if(!unsub) {
              client2.unsubscribe();
              unsub = true
            }
            if(!del) {
              client.del(`${sessionid}_client`);
              del = true
            }
            setLogs(res,ret,data,JSON.parse(message),dataSession,eventid,sessionid,ressend).then((result)=>{

              client.publish(`${sessionid}_serverA`, JSON.stringify(result), redis.print);
              client.quit();
              if(!ressend){
               res.send({msg:JSON.stringify(result)});
               ressend = true;
              }
              end = new Date().getTime();
              /*fs.readFile(`./benchmarking/${sessionid}.json`, 'utf8', function readFileCallback(err, data){
                var info = {};
                if(data) info = JSON.parse(data);
                info['Start request from server A (Server B)'] = start;
                info['Time after hashing (Server B)'] = timeafterhashing;
                info['Time getinfo from PSB server A brunch (Server B)'] = timegetinfofromPSBServerA;
                info['End request from server A (Server B)'] = end;

                fs.writeFile(`./benchmarking/${sessionid}.json`, JSON.stringify(info), 'utf8',function(){}); // write it back
              });*/
            })
            client2.quit();
          } else if(message === 'done'){
            if(!del){
              client.publish(`${sessionid}_serverA`, "done", redis.print);
            }
          }
            else if(message === 'error'){

              if(!unsub) {
                client2.unsubscribe();
                unsub = true
              }
              if(!del) {
                client.del(`${sessionid}_client`);
                del = true
              }
              client.quit();
              client2.quit();
            }
      });
      client2.subscribe(`${sessionid}_client`);

    }} else {
      var start = new Date().getTime();
      var timeresponsePSB;
      var redis = require("redis"),
      client = redis.createClient(6379, "redis"),
      client2 = redis.createClient(6379, "redis");
      console.log("log in else before redis");
            client.on("connect", function (err) {
        console.log("Redis is online ");
        });
        client.on("error", function (err) {
            console.log("Error " + err);
        });
        client2.on("connect", function (err) {
        console.log("Redis is online ");
        });
        client2.on("error", function (err) {
        console.log("Error " + err);
        });

      var body = JSON.parse(req.body.json);
      var unsub = false;
      var del = false;
      var ressend = false;
      dataSession = {
        ClientID:body.ClientID,
        Referrer:body.Referrer,
        Host:body.Host
      };

      client.on("error", function (err) {
        console.log("Error " + err);
      });
      fetch(` https://g42d5jw22c.execute-api.eu-central-1.amazonaws.com/test/psb/decrypted/${sessionid.toString()}/`,
      {
        method: 'POST',
        body: JSON.stringify(dataSession),
        headers: { 'Content-Type': 'application/json' }
      })
      .then(function(res) {
        return res.json();
      }).then(function(json){
        timeresponsePSB = new Date().getTime();


        client.on('subscribe', function (pattern, count) {
//          console.log('client from client psubscribed to ' + pattern + ', ' + count + ' total subscriptions');
          if(!del) {
            client2.publish(`${sessionid}_client`, "done", redis.print);
          }
          // setTimeout(function(){
          //   client.ping("PING",function(err,reply){
          //     if(reply){
          //         console.log("Timeout!!!!");
          //         console.log(ressend);
          //         if(!unsub) {
          //           client.unsubscribe();
          //           unsub = true
          //         }
          //         if(!del) {
          //           client2.del(`${sessionid}_client`);
          //           del = true
          //         }
          //         client.quit();
          //         client2.quit();
          //         del = true
          //         if(!ressend){
          //           console.log("Time out sending....down....")
          //           res.send({msg:'Timeout'});
          //           ressend = true;
          //         }
          //       }
          //       if(err){
          //         if(!ressend){
          //           res.send({msg:'Error connection'});
          //           ressend = true;
          //       }
          //     }
          //   })
          // },deteteTimeout);
        });
        client.on("message", function (channel, message) {
   //       console.log("sub channel " + channel + ": " + message);
          if(message!=='done'){
              if(!unsub) {
                client.unsubscribe();
                unsub = true
              }
              if(!del) {
                client2.del(`${sessionid}_serverA`);
                del = true
              }
              client2.quit();
              client.quit();
              del = true
              if(!ressend){
                mainrequest(message,req.body.json,res,start,sessionid,timeresponsePSB);
                ressend = true;
              }
          } else {
              if(!del) {
                client2.publish(`${sessionid}_client`, JSON.stringify(json), redis.print);
              }
            }
          });
        client.subscribe(`${sessionid}_serverA`);
      }).catch(function(err) {
          if(!del) {
           client2.publish(`${sessionid}_client`, "error", redis.print);
            client2.del(`${sessionid}_serverA`);
            del = true
          }
          client2.quit();
          del = true
          var log = createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'Forward','Error','B','','true');
            sendLogsToPSB(log,res,ressend);
            if(!ressend){
                res.send({msg:'Error eventID: ' +  log.EventId});
                ressend = true;
            }
      });
    }
  return next();
});

serverB.listen(9000, function() {
  console.log('%s listening at %s', serverB.name, serverB.url);
});
function mainrequest(res1,res2,res,start,sessionid,timeresponsePSB){

  var chunksLength = 0;
  var beforereq = new Date().getTime();
  var values = [JSON.parse(res2),JSON.parse(res1)];

  var headers={};
  for(let key in values[0]){
    if(key!=="Method" && key!=="Host" && key!=="Cookie"){
        headers[key] = values[0][key]
    }
    if(Array.isArray(values[1]))
        if(values[1][1])
          if(values[1][1].hasOwnProperty("value"))
            if(key==="Content-Type")
            {
              headers["Body"] = values[1][1]["value"];
            }
  }
  if(Array.isArray(values[1]))
    if(values[1][0])
      if(values[1][0].hasOwnProperty("value"))
        if(values[1][0]["value"]!==undefined)
        {
          headers["Cookie"] = [values[1][0]["value"]];
        }

  var options = {
   method: values[0]["Method"],
   url: values[0]["URL"],
   payload: null,
   headers: headers,
   disableGzip: true
 };
 var fetch = new FetchStream(values[0]["URL"],options);
 fetch.on("meta",function(meta){
  res.writeHead(meta.status,meta.responseHeaders);
 });

 fetch.on("data",function(chunk){
  res.write(chunk);

  chunksLength +=chunk.length;
 });

 fetch.on("end",function(){
  res.end();
  var end  = new Date().getTime();
  /*fs.readFile(`./benchmarking/${sessionid}.json`, 'utf8', function readFileCallback(err, data){
    var info = {};
    if(data) info = JSON.parse(data);
    info['Start request from client (Server B)'] = start;
    info['Time end request from PSB client brunch (Server B)'] = timeresponsePSB;
    info['Before main request (Server B)'] = beforereq;
    info['End request (Server B)'] = end;
    info['Data length'] = chunksLength;
    fs.writeFile(`./benchmarking/${sessionid}.json`, JSON.stringify(info), 'utf8',function(){}); // write it back
 });*/
 });
}
function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));

}

function setLogs(res,ret,data,json,dataSession,eventid,sessionid,ressend){
  return new Promise((resolve, reject)  => {
      var dpp = {dpps:[]};
      var prem = Preliminary(data,json);
      var encodeA = decodingValue(prem,dpp);
      dpp.dpps.forEach(function(item){
          if(item.hasOwnProperty("status")){
            if(item.status === "decrypted"){
              var log = createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'Forward','Info','B',item.DPP,'true');
              sendLogsToPSB(log,res,ressend);
              eventid = log.EventId;
          }
            else
            {
              var log = createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'Block','Info','B',item.DPP,'true');
              sendLogsToPSB(log,res,ressend);
              eventid = log.EventId;
            }
          }
            else
          {
            var log = createlogs(sessionid,dataSession.ClientID,dataSession.Referrer,dataSession.Host,'Block','Info','B',item.DPP,'true');
            sendLogsToPSB(log);
            eventid = log.EventId;
          }
        });
        resolve(encodeA);
  });

}
function transformation(data){
  return data.map(function(item){
    var newitem = item;
    newitem.value = item.value.split("").reverse().join("");
    delete newitem['dppHash'];
    delete newitem['dppKey'];
    return newitem;
  })
}
function Preliminary(data,dictionary){
  return data.map(function(item){
    var newitem = item;
    var dppKeys = [];
    if(Array.isArray(dictionary.key))
      dppKeys =dictionary.key.filter(function(elem){
        return elem.dppHash === item.dppHash
      })
    if(dppKeys.length === 0 ) {
      newitem['dppKey'] = null;
    }
    else {
      newitem['dppKey'] = dppKeys[0].dppKey;
    }
    return newitem;
  })
}
function hashing (data) {
  return data.map(function (item, index) {
    var ddpItem = item;
    ddpItem['dppHash'] = ourCryptoB.text2Hash(item['DPP']);
    return ddpItem
  });
}

function decodingValue (data,dpp) {
//  var absolutePath = path.resolve(keyPrivB);
//  var privateKeyB = fs.readFileSync(absolutePath, "utf8");
  return data.map(function (item) {
    var newitem = item;
    if(newitem['dppKey'] === null){
      dpp.dpps.push({DPP:newitem['DPP'],status:"not decrypted"});
      newitem['value']=newitem['dppEncode'];
    }
    else{
      dpp.dpps.push({DPP:newitem['DPP'],status:"decrypted"});
      newitem['value']=ourCryptoB.decryptKeyVal(newitem['dppKey'], newitem['dppEncode'], newitem['dppDigest']);
    }
    delete newitem['dppKey'];
    delete newitem['dppEncode'];
    delete newitem['dppHash'];
    delete newitem['dppDigest'];
    return newitem
  })
}

function sendLogsToPSB(logDB,res,ressend){

  fetch( 'https://a40llf38y1.execute-api.eu-central-1.amazonaws.com/dev/psb/logs_add',
  {
    method: 'POST',
    body: JSON.stringify(logDB),
    headers: { 'Content-Type': 'application/json' }
  })
  .then(function (result) {
    return result.json()
  }).then(function (json) {
    console.log("Done logs to server PSB request");
  //  console.log(json);
  }).catch(function(err) {
      console.log(err);
  });
}
function niceEncodeObject (data)
{
       return encodeURIComponent(data).replace(/[ !'()*]/g,
               function(c) { return '%' + c.charCodeAt(0).toString(16); });
}
function arraytodata(res){
  var enc = new TextDecoder();
  return enc.decode(res);
}
